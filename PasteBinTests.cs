#pragma warning disable NUnit1032
using NUnit.Framework;
using NUnit.Framework.Legacy;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using System;

namespace PasteBinTest
{
    public class Tests
    {
        IWebDriver driver;

        [OneTimeSetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            
        }

        [Test]
        public void CreateNewPaste()
        {
            var pasteBinPage = new PasteBinPage(driver);

            pasteBinPage.Navigate();
            pasteBinPage.CreateNewPaste(
                code: "Hello from WebDriver",
                expiration: "10 �����",
                name: "helloweb"
            );


#pragma warning disable NUnit2005
            ClassicAssert.AreEqual("PrivateBin", driver.Title);
#pragma warning restore NUnit2005
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            driver?.Quit();
            driver?.Dispose();
        }
    }
}
