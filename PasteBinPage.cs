﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace PasteBinTest
{
    public class PasteBinPage
    {
        private readonly IWebDriver driver;

        public PasteBinPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void Navigate()
        {
            driver.Navigate().GoToUrl("https://privatebin.net/");
        }

        public void CreateNewPaste(string code, string expiration, string name)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            IWebElement codeTextArea = wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("message")));
            codeTextArea.SendKeys(code);

            MakeElementVisible("pasteExpiration");

            IWebElement expirationDropdown = wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("pasteExpiration")));
            SelectElement dropDown = new SelectElement(expirationDropdown);

            dropDown.SelectByValue("10min");
        }

        private void MakeElementVisible(string elementId)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("document.getElementById('" + elementId + "').classList.remove('hidden');");
        }
    }
}